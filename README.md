#Assignment 3

In this assignment I am first making a POST request for 10 id's. The insertion of data can happen for every id to any of the three database . The insertion is decided upon the port id returned by the hash function. With the help of hash function I am able to decide which port should I insert a particular id. Every server is listening to 3 different port.
Hence 10 POST request are being distributed among the 3 server I have created. The Consistent hashing is giving the port number to be used for insertion for ever POST request for the id.

Once POST has been done, I am performing GET request.
10 GET request is made individually for all the 3 server which is listening to 3 different port(5001,5002,5003). This servers are connected to three different Mysql database.

Below output shows the get request for which value was present in the particular database, which was inserted during POST request give 404 non found error if request is not found. 

Total of 30 GET request was made. 10 for each server.

##Steps to run the program in system.

The entire setup was done on docker. If you want to run the program in your local system then ,

 - Change the HOSTNAME in the database file to "localhost".
 - Run the app.py ,app1.py, and app2.py before running client.py as  client.py sends request to three different server which are here(app,app1,app2).
 - POST request insertion will not happen to the id which has been already added in the databases, as the id's are primary key. Make sure to change the id every time you make a post request.


#GET request output
##When made 10 GET request with PORT 5001

~~~
localhost:5001
/v1/expenses/1
{
  "category": "office supplies", 
  "decision_date": "09-10-2016", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 1, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "pending", 
  "submit_date": "12-10-2016"
}


localhost:5001
/v1/expenses/2
{
  "category": "office supplies", 
  "decision_date": "09-10-2016", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 2, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "pending", 
  "submit_date": "12-10-2016"
}

localhost:5001
/v1/expenses/3
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

localhost:5001
/v1/expenses/4
{
  "category": "office supplies", 
  "decision_date": "09-10-2016", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 4, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "pending", 
  "submit_date": "12-10-2016"
}


localhost:5001
/v1/expenses/5
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>


localhost:5001
/v1/expenses/6
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>


localhost:5001
/v1/expenses/7
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>


localhost:5001
/v1/expenses/8
{
  "category": "office supplies", 
  "decision_date": "09-10-2016", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 8, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "pending", 
  "submit_date": "12-10-2016"
}


localhost:5001
/v1/expenses/9
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

localhost:5001
/v1/expenses/10
{
  "category": "office supplies", 
  "decision_date": "09-10-2016", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 10, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "pending", 
  "submit_date": "12-10-2016"
}

~~~




##When made 10 GET request with PORT 5002

~~~

localhost:5002
/v1/expenses/1
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

localhost:5002
/v1/expenses/2
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

localhost:5002
/v1/expenses/3
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

localhost:5002
/v1/expenses/4
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

localhost:5002
/v1/expenses/5
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

localhost:5002
/v1/expenses/6
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

localhost:5002
/v1/expenses/7
{
  "category": "office supplies", 
  "decision_date": "09-10-2016", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 7, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "pending", 
  "submit_date": "12-10-2016"
}

localhost:5002
/v1/expenses/8
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

localhost:5002
/v1/expenses/9
{
  "category": "office supplies", 
  "decision_date": "09-10-2016", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 9, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "pending", 
  "submit_date": "12-10-2016"
}

localhost:5002
/v1/expenses/10
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

~~~



##When made 10 GET request with PORT 5003

~~~
localhost:5003
/v1/expenses/1
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

localhost:5003
/v1/expenses/2
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

localhost:5003
/v1/expenses/3
{
  "category": "office supplies", 
  "decision_date": "09-10-2016", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 3, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "pending", 
  "submit_date": "12-10-2016"
}

localhost:5003
/v1/expenses/4
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

localhost:5003
/v1/expenses/5
{
  "category": "office supplies", 
  "decision_date": "09-10-2016", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 5, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "pending", 
  "submit_date": "12-10-2016"
}

localhost:5003
/v1/expenses/6
{
  "category": "office supplies", 
  "decision_date": "09-10-2016", 
  "description": "iPad for office use", 
  "email": "foo1@bar.com", 
  "estimated_costs": "700", 
  "id": 6, 
  "link": "http://www.apple.com/shop/buy-ipad/ipad-pro", 
  "name": "Foo 1", 
  "status": "pending", 
  "submit_date": "12-10-2016"
}

localhost:5003
/v1/expenses/7
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

localhost:5003
/v1/expenses/8
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

localhost:5003
/v1/expenses/9
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

localhost:5003
/v1/expenses/10
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>

~~~