import httplib
import json
from hashing import ConsistentHashRing
ring = ConsistentHashRing()


#adding server to consistent hashing

ring.__setitem__("instance1","5001")
ring.__setitem__("instance2","5002")
ring.__setitem__("instance3","5003")

#POST request
for i in xrange(1,11):
    server_port = ring.__getitem__(str(i))
    url = "localhost:" + str(server_port)
    server_connection = httplib.HTTPConnection(url)

    header = {'Content-type': 'application/json'}
    req = {
        "id": i,
        "name": "Foo 1",
        "email": "foo1@bar.com",
        "category": "office supplies",
        "description": "iPad for office use",
        "link": "http://www.apple.com/shop/buy-ipad/ipad-pro",
        "estimated_costs": "700",
        "submit_date": "12-10-2016"
    }
    json_req = json.dumps(req)
    url2 = "/v1/expenses"
    server_connection.request('POST', url2, json_req, header)
    response = server_connection.getresponse()
    print(response.read().decode())


