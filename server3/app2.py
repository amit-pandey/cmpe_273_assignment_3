import flask
from flask import Flask, jsonify
from flask import request
from Database2 import db
from Database2 import Order
from Database2 import CreateDB


app = Flask(__name__)
CreateDB()
db.create_all()

@app.route('/')
def hello():
	return ("hello , welcome to python!!'")


@app.route('/v1/expenses/<int:expenses_id>')
def show(expenses_id):
	user = Order.query.filter_by(id = expenses_id).first_or_404()
	return jsonify({ 'id': user.id, 'name': user.name, 'email': user.email , 'category': user.category, 'description': user.description, 'link': user.link ,
					 'estimated_costs': user.estimated_costs, 'submit_date':user.submit_date , 'status': user.status, 'decision_date': user.decision_date})


@app.route('/v1/expenses' , methods=['POST'])
def insert_user():
	object=request.get_json(force=True)
	user = Order(object['id'],object['name'],object['email'], object['category'],object['description'],object['link'],object['estimated_costs'], object['submit_date'], 'pending', '09-10-2016')
	db.session.add(user)
	db.session.commit()
	flask.Response(status=204)
	return jsonify({'id': user.id, 'name': user.name, 'email': user.email, 'category': user.category,
					'description': user.description, 'link': user.link,
					'estimated_costs': user.estimated_costs, 'submit_date': user.submit_date, 'status': user.status,
					'decision_date':user.decision_date}), 201

@app.route('/v1/expenses/<expenses_id>' ,methods=['PUT'])
def modify(expenses_id):
	object1 = request.get_json(force=True)
	user = Order.query.filter_by(id=expenses_id).first_or_404()
	user.estimated_costs=object1['estimated_costs']
	db.session.commit()
	return jsonify({"Status" : "true"}), 202





@app.route('/v1/expenses/<expenses_id>' ,methods=['DELETE'])
def delete(expenses_id):
	user = Order.query.filter_by(id=expenses_id).first_or_404()
	db.session.delete(user)
	db.session.commit()

	resp = flask.Response()
	resp.status_code= 204
	return resp



if __name__ == "__main__":
	app.run(host="0.0.0.0", port=5003)





